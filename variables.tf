variable "applicance_name" {
  type        = string
  description = "The name of the appliance"
}

variable "appliance_number" {
  type        = string
  description = "Number of appliances to be deployed set by the Autoscaling group."
  default     = "1"
}

variable "assign_public_ip" {
  type        = bool
  description = "Determine whether or not The EC2 Instances should have a Public IP Address assigned to it"
  default     = false
}

variable "cloudformation_capabilities" {
  type        = list(string)
  description = "(Optional) A list of capabilities. Valid values: CAPABILITY_IAM, CAPABILITY_NAMED_IAM, or CAPABILITY_AUTO_EXPAND"
  default     = [
    "CAPABILITY_AUTO_EXPAND"
  ]
}

variable "enabled" {
  type        = bool
  description = "Enable all resources in this module"
  default     = true
}

variable "deploy_ids" {
  type        = bool
  description = "Deploy the AlertLogic IDS"
  default     = false
}

variable "disable_rollback" {
  type        = bool
  description = "Disable CloudFormation's Rollback on Failure"
  default     = false
}

variable "deploy_scanner" {
  type        = bool
  description = "Deploy the AlertLogic Scanner"
  default     = false
}

variable "iam_role_arn" {
  type        = string
  description = "(Optional) The AWS IAM ARN that CloudFormation should use"
  default     = ""
}

variable "subnet_ids" {
  type        = list(string)
  description = "Specify the existing subnet ID where the Scanning appliance will be deployed in."
}

variable "subnet_type" {
  type        = string
  description = "Select if the subnet is a public or private subnet. Enter Public or Private"
}

variable "instance_type" {
  type        = string
  description = "AlertLogic Security Appliance EC2 instance type."
}

variable "notification_arns" {
  type        = list(string)
  description = " (Optional) A list of SNS topic ARNs to publish stack related events."
  default     = []
}

variable "tags" {
  type        = map(any)
  description = "(Optional) Map of resource tags to associate with this stack."
  default     = null
}

variable "timeout_in_minutes" {
  type        = string
  description = "(Optional) The amount of time that can pass before the stack status becomes CREATE_FAILED."
  default     = "0"
}

variable "use_cfn_template_url" {
  type        = bool
  description = "Use the CloudFormation Templates direct from AlertLogic's site"
  default     = false
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR netblock for the VPC."
}

variable "vpc_id" {
  type        = string
  description = "Specify the VPC ID where the appliance will be deployed in."
}

variable "ids_template_url" {
  type        = string
  description = "The location of the CloudFormation Template on the ALertLogic Site"
  default     = "https://s3.amazonaws.com/cd.prod.manual-mode.repository/cf_templates_latest/al_ids.yaml"
}

variable "scanner_template_url" {
  type        = string
  description = "The location of the CloudFormation Template on the ALertLogic Site"
  default     = "https://s3.amazonaws.com/cd.prod.manual-mode.repository/cf_templates_latest/al_scan.yaml"
}
