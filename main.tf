locals {
  ids_template_body     = file("${path.module}/include/al_ids.yaml")
  ids_cfn_template      = var.use_cfn_template_url ? var.ids_template_url : local.ids_template_body
  scanner_template_body = file("${path.module}/include/al_scan.yaml")
  scanner_cfn_template  = var.use_cfn_template_url ? var.scanner_template_url : local.scanner_template_body
  cfn_template          = var.deploy_scanner ? local.scanner_cfn_template : var.deploy_ids ? local.ids_cfn_template : null
}

resource "aws_cloudformation_stack" "main" {
  count               = var.enabled && (var.deploy_scanner || var.deploy_ids) ? 1 : 0
  name                = var.applicance_name
  capabilities        = var.cloudformation_capabilities
  disable_rollback    = var.disable_rollback
  iam_role_arn        = var.iam_role_arn
  notification_arns   = var.notification_arns
  tags                = var.tags
  timeout_in_minutes  = var.timeout_in_minutes 
  template_body       = var.use_cfn_template_url ? null : local.cfn_template
  template_url        = var.use_cfn_template_url ? local.cfn_template : null

  parameters = {
        VpcId          = var.vpc_id
        VpcCidr        = var.vpc_cidr
        SubnetId       = join(",", var.subnet_ids) # Because the CloudFormation Template requires a string and not a list of strings
        InstanceType   = var.instance_type
        NumAppliances  = var.appliance_number
        AssignPublicIp = var.assign_public_ip
  }
}
