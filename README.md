# tf-aws-alertlogic
The purpose of this Terraform Module is to help with the provisioning of the AlertLogic Scanning and IDS resources in an AWS Account.

This is essentially a wrapper for various CloudFormation Stacks which perform the provisioning of the resources.

## Features

Terraform will create a CloudFormation Stack which creates and manages the required resources.

Cloudformation will create the following resources:
* Security groups with the required Security Group Rules to allow it to communicate with the enviroment and also AlertLogic.
* An EC2 Autoscaling Group
* An EC2 Launch Template
* One or more EC2 instances which are managed by the AUtoscaling Group

## Authors and acknowledgment
The CloudFormation Templates where provided by AlertLogic.

The Origional CloudFormation Templates are located at:
* https://s3.amazonaws.com/cd.prod.manual-mode.repository/cf_templates_latest/al_ids.yaml
* https://s3.amazonaws.com/cd.prod.manual-mode.repository/cf_templates_latest/al_scan.yaml

AlertLogic's CloudFormation Templates where copied into this module repository on 9th February 2024.

## Variables
|Name|Description|Type|Default|Required|
|---|---|---|---|---|
|appliance_name|The name of the appliance|string||Yes|
|appliance_number|Number of appliances to be deployed set by the Autoscaling group|string|1|No|
|assign_public_ip|Determine whether or not The EC2 Instances should have a Public IP Address assigned to it|boolean|false|No|
|cloudformation_capabilities|A list of capabilities. Valid values: CAPABILITY_IAM, CAPABILITY_NAMED_IAM, or CAPABILITY_AUTO_EXPAND"|list(string)|CAPABILITY_AUTO_EXPAND|No|
|enabled|Enable all resources in this module|boolean|true|No|
|deploy_ids|Deploy the AlertLogic IDS|boolean|false|No|
|deploy_scanner|Deploy the AlertLogic Scanner|boolean|false|No|
|disable_rollback|Disable CloudFormation's Rollback on Failure|boolean|false|No|
|iam_role_arn|The AWS IAM ARN that CloudFormation should use|string||No|
|ids_template_url|The location of the CloudFormation Template on the AlertLogic Site|string|https://s3.amazonaws.com/cd.prod.manual-mode.repository/cf_templates_latest/al_ids.yaml|No|
|subnet_ids|Specify the existing subnet ID where the Scanning appliance will be deployed in|list(string)||Yes|
|subnet_type|Select if the subnet is a public or private subnet. Enter Public or Private|string||Yes|
|instance_type|AlertLogic Security Appliance EC2 instance type|string||Yes|
|notification_arns|A list of SNS topic ARNs to publish stack related events|list(string)||No|
|on_failure|Action to be taken if stack creation fails. This must be one of: DO_NOTHING, ROLLBACK, or DELETE|string|ROLLBACK|No|
|scanner_template_url|The location of the CloudFormation Template on the AlertLogic Site|string|https://s3.amazonaws.com/cd.prod.manual-mode.repository/cf_templates_latest/al_scan.yaml|No|
|tags|Map of resource tags to associate with this stack|map(any)||No|
|timeout_in_minutes|The amount of time that can pass before the stack status becomes CREATE_FAILED|string|60|No|
|use_cfn_template_url|Use the CloudFormation Templates direct from AlertLogic's site|bool|false|No|
|vpc_cidr|CIDR netblock for the VPC|string||Yes|
|vpc_id|Specify the VPC ID where the appliance will be deployed in|string||Yes|
